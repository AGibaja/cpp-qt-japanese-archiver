#include "MainWindow.hpp"

#include <QApplication>

#include <QTabWidget>
#include <QFrame>
#include <QTabBar>

#include <QLabel>
#include <QVBoxLayout>
#include <QDebug>
#include <QLineEdit>

#include <stdio.h>
#include <iostream>

#include <TabsWidget.hpp>
#include <CreationView.hpp>
#include <HiraKataView.hpp>

#include <sqlite3.h>


int callback(void *, int, char **, char **);



int main(int argc, char *argv[])
{

    sqlite3 *db;

    //const char *command = "CREATE TABLE Hiragana(original varchar, romaji varchar)";

    const char *command_2 = "INSERT INTO Hiragana VALUES('KA', 'IN ROMAJI')";
    const char *command_3 = "SELECT * FROM Hiragana;";

    char *error = 0;

    int rc = sqlite3_open("./test.db", &db);

    if (rc != SQLITE_OK)
    {
        //qDebug() << "Error opening database.";
    }

    else
    {
        //qDebug() << "Db opened.";

        rc = sqlite3_exec(db, command_2, callback, 0, &error);
        rc = sqlite3_exec(db, command_3, callback, 0, &error);

    }


    /*QApplication a(argc, argv);


    QFrame *central_widget = new QFrame();
    central_widget->setLayout(new QVBoxLayout());


    TabsWidget *tab = new TabsWidget(central_widget);
    central_widget->layout()->addWidget(tab);


    HiraKataView *hk_view = nullptr;
    CreationView *kanji_view  = nullptr;
    CreationView *word_view = nullptr;

    QString name = "";
    QString style = "";

    for (int i = 0; i < 4; ++i)
    {
        switch (i)
        {

        case 0:
        {
            name = "Hiragana/Katakana";
            hk_view = new HiraKataView (tab, true);
            tab->addTab(hk_view, name);
            break;
        }

        case 1:
        {
            name = "Kanji";
            kanji_view = new HiraKataView (tab, false);
            tab->addTab(kanji_view, name);
            break;
        }

        case 2:
        {
            name = "Word";
            word_view = new HiraKataView (tab, false);
            tab->addTab(word_view, name);
            break;
        }

        default:
        {
            name = "Error";
            break;
        }

        }

    }


    MainWindow w;
    w.setCentralWidget(central_widget);
    w.resize(800, 600);


    w.show();



    return a.exec();
    */
}


int callback(void *not_used, int argc, char **argv, char **col_name)
{
    not_used = 0;

    for (int i = 0; i < argc; i++)
    {
        auto column_name = col_name[i];
        std::cout << "Column name: " << column_name << std::endl;
        auto value = argv [i];
        std::cout << "Value: " << value << std::endl;
    }

    return 0;
}

