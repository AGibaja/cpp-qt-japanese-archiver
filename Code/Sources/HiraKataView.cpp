#include "HiraKataView.hpp"

#include <QTableWidget>
#include <QPushButton>
#include <QDebug>
#include <QLineEdit>

#include <string>


HiraKataView::HiraKataView(QWidget *parent, bool is_hira)
    : CreationView(parent)
{
    if (is_hira)
        this->is_hiragana = true;
    else
        this->is_katakana = true;

    this->initLayouts();
    this->initHkView();
}


HiraKataView::~HiraKataView()
{

}


void HiraKataView::initHkView()
{

    if (this->is_hiragana)
    {
        QString str = "Hiragana Creation";
        this->setTextViewType(str);
    }

    else
    {
        QString str = "Katakana Creation";
        this->setTextViewType(str);
    }



}


void HiraKataView::initLayouts()
{
    this->contents = new QFrame();

    this->v_layout = new QHBoxLayout();
    this->contents->setLayout(this->v_layout);

    this->initFont();
    this->initLeftLayout();
    this->initRightLayout();

    this->getCreationFrame()->layout()->addWidget(this->contents);
}


void HiraKataView::initLeftLayout()
{
    this->left_layout = new QVBoxLayout();
    this->left_layout->setAlignment(Qt::AlignTop);
    this->left_contents = new QFrame();
    this->left_contents->setObjectName("LeftContents");
    this->left_contents->setStyleSheet("#LeftContents { border : 1px solid gray; border-radius : 4px; border-color : #69b0d1; }");
    this->left_layout->addWidget(this->left_contents);
    this->left_contents->setLayout(new QVBoxLayout());

    this->initLeftContents();

    this->v_layout->addLayout(this->left_layout);
}


void HiraKataView::initRightLayout()
{
    this->right_layout = new QVBoxLayout();
    this->right_layout->setAlignment(Qt::AlignTop);
    this->right_contents = new QFrame();
    this->right_contents->setObjectName("RightContents");
    this->right_contents->setStyleSheet(" #RightContents { border : 1px solid gray; border-radius : 4px; border-color : #69b0d1 ; }");
    this->right_layout->addWidget(this->right_contents);
    this->right_contents->setLayout(new QVBoxLayout());

    this->initRightContents();

    this->v_layout->addLayout(this->right_layout );
}


void HiraKataView::initLeftContents()
{
    QLabel *label_original_hiragana = new QLabel("Original Hiragana");
    label_original_hiragana->setFont(label_font);

    this->original_hiragana_edit = new QLineEdit();
    this->original_hiragana_edit->setStyleSheet("QLineEdit {border: 1px solid; border-radius : 4px; border-color : #807e7e; }");

    this->left_contents->layout()->addWidget(label_original_hiragana);
    this->left_contents->layout()->addWidget(this->original_hiragana_edit);

    QLabel *label_romaji_equivalent = new QLabel("Romaji Equivalent");
    label_romaji_equivalent->setFont(this->label_font);

    this->romaji_hiragana_edit = new QLineEdit();
    this->romaji_hiragana_edit->setStyleSheet("QLineEdit {border: 1px solid; border-radius : 4px; border-color : #807e7e; }");

    this->left_contents->layout()->addWidget(label_romaji_equivalent);
    this->left_contents->layout()->addWidget(this->romaji_hiragana_edit);

    this->create_hiragana_button = new QPushButton("Create Hiragana");
    connect(create_hiragana_button, SIGNAL(clicked()), this, SLOT(checkHiraganaIntroduced()));
    this->left_contents->layout()->addWidget(this->create_hiragana_button);
}


void HiraKataView::initRightContents()
{
    QLabel *label_original_katakana = new QLabel("Original Katakana");
    label_original_katakana->setFont(this->label_font);

    this->original_katakana_edit = new QLineEdit();
    this->original_katakana_edit->setStyleSheet("QLineEdit {border: 1px solid; border-radius : 4px; border-color : #807e7e; }");

    this->right_contents->layout()->addWidget(label_original_katakana);
    this->right_contents->layout()->addWidget(this->original_katakana_edit);

    QLabel *label_romaji_equivalent = new QLabel("Romaji Equivalent");
    label_romaji_equivalent->setFont(this->label_font);

    this->romaji_katakana_edit = new QLineEdit();
    this->romaji_katakana_edit->setStyleSheet("QLineEdit {border: 1px solid; border-radius : 4px; border-color : #807e7e; }");

    this->right_contents->layout()->addWidget(label_romaji_equivalent);
    this->right_contents->layout()->addWidget(this->romaji_katakana_edit);

    this->create_katakana_button = new QPushButton("Create Katakana");
    connect(create_katakana_button, SIGNAL(clicked()), this, SLOT(checkKatakanaIntroduced()));
    this->right_contents->layout()->addWidget(this->create_katakana_button);
}


void HiraKataView::initFont()
{
    this->label_font.setPixelSize(14);
    this->label_font.setFamily("Roboto");
    qDebug() << "Font: " << this->label_font.family();
    this->label_font.setBold(true);
}


void HiraKataView::checkHiraganaIntroduced()
{
    qDebug() << "Text introduced in hiragana original: " << this->original_hiragana_edit->text();
    this->hiragana_original_text = this->original_hiragana_edit->text().toUtf8();
    qDebug() << "Text introduced in romaji hiragana: " << this->romaji_hiragana_edit->text();
    this->hiragana_romaji_text = this->romaji_hiragana_edit->text();
    QString last_introduced = this->hiragana_original_text + ", " + this->hiragana_romaji_text + " ";
    this->getDebugLabel()->setText("Last introduced: " + last_introduced);

    if (this->hiragana_original_text.isEmpty() || this->hiragana_romaji_text.isEmpty())
        this->getDebugLabel()->setText("One of two values is empty.");

}


void HiraKataView::checkKatakanaIntroduced()
{
    qDebug() << "Text introduced in katakana original: " << this->original_katakana_edit->text();
    this->katakana_original_text = this->original_katakana_edit->text();
    qDebug() << "Text introduced in romaji katakana: " << this->romaji_katakana_edit->text();
    this->katakana_romaji_text = this->romaji_katakana_edit->text();
    QString last_introduced = this->katakana_original_text + ", " + this->katakana_romaji_text + " ";
    this->getDebugLabel()->setText("Last introduced: " + last_introduced);

    if (this->katakana_original_text.isEmpty() || this->katakana_romaji_text.isEmpty())
        this->getDebugLabel()->setText("One of two values is empty.");

}

