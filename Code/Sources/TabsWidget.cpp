#include <TabsWidget.hpp>

#include <QTabBar>
#include <QTabWidget>

#include <iostream>


TabsWidget::TabsWidget(QWidget *parent)
    : QTabWidget(parent)
{
    this->setCustomAppearance();
    connect(this, SIGNAL(currentChanged(int)), this, SLOT(onSelected(int)));
}


TabsWidget::~TabsWidget()
{

}


void TabsWidget::setStyleSheet(QString &sheet)
{
    this->style_sheet = sheet;
}


QString &TabsWidget::getStyleSheet()
{
    return this->style_sheet;
}


void TabsWidget::setCustomAppearance()
{
    this->style_sheet = "QTabBar::tab{border-bottom : 0px solid red; margin-right: 10px;} "
                        "QTabBar::tab::selected {border-bottom : 3px solid #47788f; }";
    QTabWidget::setStyleSheet(this->style_sheet);
    std::cout << "Styles set." << std::endl;

}


void TabsWidget::onSelected(int selected)
{

}
