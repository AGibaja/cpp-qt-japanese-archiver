#include "CreationView.hpp"

#include <QFrame>
#include <QDebug>
#include <QPushButton>



CreationView::CreationView(QWidget *parent)
    : QFrame(parent)
{

    this->initLabel();
    this->initCommonFrame();
    this->initDebugLabel();

}


CreationView::~CreationView()
{

}


void CreationView::setTextViewType(QString &text)
{
    this->label->setText(text);
}


std::shared_ptr<QFrame> CreationView::getCreationFrame()
{
    return this->common_frame;
}


std::shared_ptr<QLabel> CreationView::getDebugLabel()
{
    return this->debug_label;
}


void CreationView::initLabel()
{
    this->label = std::make_shared<QLabel>();
    this->label->setText("Generic Creation");

    this->label->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    this->label->setMaximumHeight(50);


    QFont f("Ubuntu", 20, QFont::Light, false);
    this->label->setFont(f);

    this->v_box_layout->addWidget(label.get());

}


void CreationView::initCommonFrame()
{
    this->common_frame->setParent(this);
    this->common_frame->setLayout(new QVBoxLayout());

    this->v_box_layout->addWidget(this->common_frame.get());


    /// \warning Possible double delete to this pointer since ownership is of Qt and
    ///          of v_box_layout.
    this->setLayout(this->v_box_layout.get());
}


void CreationView::initDebugLabel()
{
    this->debug_label->setObjectName("DebugLabel");
    this->debug_label->setStyleSheet("#DebugLabel { background-color: #cfd2d4; border-radius: 8px;} ");
    this->debug_label->setMinimumHeight(250);
    this->v_box_layout->layout()->addWidget(this->debug_label.get());
    this->v_box_layout->setAlignment(this->debug_label.get(), Qt::AlignBottom);
}

