#ifndef ELCREATIONVIEW_HPP
#define ELCREATIONVIEW_HPP

#include <QWidget>
#include <QLabel>

#include <memory>

#include <QVBoxLayout>


class QFrame;

class CreationView : public QFrame
{
Q_OBJECT

public:
    CreationView(QWidget *w = nullptr);

    virtual ~CreationView();


protected:
    void setTextViewType(QString &text);

    std::shared_ptr<QFrame> getCreationFrame();

    std::shared_ptr<QLabel> getDebugLabel();


private:
    ///Initialize label that will tell the user what type of creation
    /// window it is.
    void initLabel();

    ///Central frame where each subclass will show its own element
    ///creation frame.
    void initCommonFrame();
    void initDebugLabel();


    std::shared_ptr<QFrame> common_frame =
            std::make_shared<QFrame>();

    std::shared_ptr<QLabel> label = nullptr;

    std::shared_ptr<QVBoxLayout> v_box_layout =
            std::make_shared<QVBoxLayout>();

    std::shared_ptr<QLabel> debug_label =
            std::make_shared<QLabel>();


};

#endif // ELCREATIONVIEW_HPP
