#ifndef HIRAKATAVIEW_HPP
#define HIRAKATAVIEW_HPP

#include <CreationView.hpp>


class QPushButton;
class QTableWidget;
class QLineEdit;


class HiraKataView : public CreationView
{
Q_OBJECT

public:
    HiraKataView(QWidget *parent = nullptr, bool is_hira = true);

    ~HiraKataView();


private:

    bool is_hiragana = false;
    bool is_katakana = false;

    QHBoxLayout *v_layout= nullptr;

    QVBoxLayout *left_layout= nullptr;
    QVBoxLayout *right_layout= nullptr;

    QFrame *contents = nullptr;
    QFrame *left_contents = nullptr;
    QFrame *right_contents = nullptr;

    QLabel *value_label = nullptr;
    QLabel *meaning_label = nullptr;

    QLineEdit *original_hiragana_edit = nullptr;
    QLineEdit *romaji_hiragana_edit = nullptr;
    QLineEdit *original_katakana_edit = nullptr;
    QLineEdit *romaji_katakana_edit = nullptr;

    QPushButton *create_hiragana_button = nullptr;
    QPushButton *create_katakana_button = nullptr;

    QFont label_font;

    QString hiragana_original_text = "";
    QString hiragana_romaji_text = "";
    QString katakana_original_text = "";
    QString katakana_romaji_text = "";


    void initHkView();
    void initLayouts();
    void initLeftLayout();
    void initRightLayout();
    void initLeftContents();
    void initRightContents();
    void initFont();
    void connectEdits();


public slots:
    void checkHiraganaIntroduced();
    void checkKatakanaIntroduced();


};

#endif // HIRAKATAVIEW_HPP
