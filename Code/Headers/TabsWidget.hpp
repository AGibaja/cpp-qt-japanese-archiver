#ifndef CUSTOMTAB_HPP
#define CUSTOMTAB_HPP

#include <QTabWidget>


class TabsWidget : public QTabWidget
{
Q_OBJECT

public:
    TabsWidget(QWidget *widget = nullptr);

    virtual ~TabsWidget();

    void setStyleSheet(QString &sheet);

    QString &getStyleSheet();


private:
    int selected = 0;

    QString style_sheet;


    void setCustomAppearance();


private slots:
    void onSelected(int index);


};

#endif // CUSTOMTAB_HPP
